#### Configuration Management#####

------------- Summary -------------
This directory includes all the information regarding how we maintained Configuration Management for our project

------------- Documents -------------
- Configuration Management: This document outlines how we versioned our documents, how we version-controlled the code base, and how we collaborate.

- VDM 2017 Template: This document is the template from which all of our documents pertaining to the project should extend.

------------- Resources Subdirectory -------------
This includes any additional resources, notably documents from the previous team in their raw format. We included the previous team's configuration management document because we built off of theirs for consistency.