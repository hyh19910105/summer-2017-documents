#### Confluence Documents #####

------------- Summary -------------
This directory includes all the information for meeting notes that we got from confluence.

------------- Sub-directories -------------
- Client Meeting Notes: These are the weekly client meeting notes.

- Mentor Meeting Notes: These are weekly mentor meeting notes

- Retrospectives: These are bi-weekly retrospective meeting notes.