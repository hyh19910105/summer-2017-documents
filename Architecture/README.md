#### Architecture #####

------------- Summary -------------
This directory includes all the information regarding the architecture

------------- Documents -------------
- Architecture Document: This document outlines the conformance check and evaluation we did for the architecture.

------------- Sub-Directories -------------
The subdirectory containing Resources includes our raw architecture notes.