#### Context and Requirements Management #####

------------- Summary -------------
This directory includes all the information regarding the context and scope of the project as well as the requirements.

------------- Documents -------------
- Requirement Document: This is a copy of the Alpha Team's original requirements specification. We have allowed it to remain a living and breathing document by building on top of it. Although the requirements are stable and not likely to change based on our talks with the client, we still need a requirements specification document that can outline a requirements management strategy as well as provide a clear, up-to-date view of the requirements since they have changed between the time Alpha Team finished their work, and when we incepted the project.

- Requirements Evaluation: This document is used to evaluate the Requirements Specification of the Alpha Team. Since our project is not a greenfield project, we needed to produce a document to capture the outstanding questions we had and any clarifications needed from the client. We also used this document to include information about our findings so that we have a quick reference for the future about the original document. This document is not a "living and breathing" document, i.e. we will not be updating it after we get our open questions answered.

- Statement of Work: This document defines project-specific activities, deliverables, timelines, and specifics about logistics pertaining to the services we will provide to the client. We will be using this to define the scope and the context as well, and it will help manage our client's expectations as well as our own.

- Test Plan: This defines our plan for executing the testing phase of our project as well as the requirements for testing. The purpose and goal for having the document is to be able to clearly define what it means to be "done testing." We worked with the client and systematically developed a plan to test the system.

- Acceptance Testing Documentation: This document is used to document the process of acceptance testing, release history and acceptance testing feedback.

------------- Resources -------------
- Github Evaluation: This is a tech stack evaluation that was done during our requirements elicitation process and was used to preemptively understand the requirements for the future adapter we will build. This will define upfront how we approach and think about the system design moving forward.

- Alpha Team Documents: We included the previous team's specification documents and Requirements Process documents for reference.