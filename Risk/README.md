#### Risk #####

------------- Summary -------------
This directory includes all the information regarding anything pertaining to risks and our mitigation strategies.

------------- Documents -------------
- Risk Document: This document outlines how we identified risks, how we managed them, and how we mitigated them.

------------- Previous Versions -------------
This subdirectory includes previous versions of the risk documents. We included the versions for risk documents only to demonstrate how our risks evolved over time.