#### Project Planning & Tracking #####

------------- Summary -------------    
This directory includes all the information regarding the project planning and tracking

------------- Documents -------------    
- Planning and Tracking: This document outlines how we plan and track our project progress.

- MSIT Project Milestone Plan: This is a continuously evolving and updating milestone plan. Details about what it means can be found in Planning and Tracking.

- Project Tracking and EV Chart: This is a continuously evolving and updating sprint by sprint project plan and Earned Value Chart. Details about what it means can be found in Planning and Tracking.

------------- Sub-Directories -------------     
The subdirectory containing Resources includes our raw retrospective notes.