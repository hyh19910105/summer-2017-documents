#### Miscellaneous Artifacts #####

------------- Summary -------------
This directory includes all the artifacts that do not belong in one of the core practice area directories. It may include other relevant artifacts.

------------- Documents -------------
- 2017-02-27 Knowledge Transfer Meeting Notes: This document includes raw notes for the knowledge transfer meeting we had with Team Project Dashboard's previous teammate, Anurag Kanungo.

- JIRA Ticket Template: This is the template we use when creating JIRA tickets.

- Planning Meeting Checklist: This is a checklist we go through to help us structure our weekly planning meetings.