# Adapter & Log Info

### JIRA adapter

|Listen to port|Log file location|
|:---:|:---:|:---:|
|5656|**/var/tmp/jiraserver.log**|

### Bamboo adapter

|Listen to port|Log file location|
|:---:|:---:|:---:|
|5657|**/var/tmp/bamboo.log**|

### Bitbucket adapter

|Listen to port|Log file location|
|:---:|:---:|:---:|
|5658|**/var/tmp/bitbucket.log**|

### Fogbugz adapter

|Listen to port|Log file location|
|:---:|:---:|:---:|
|5659|**/var/tmp/fogbugz.log**|



# System module
### Configration page

|Listen to port|Log file location|
|:---:|:---:|:---:|
|-|**/var/log/configurationpage.log**|

### Monitor service

|Listen to port|Log file location|
|:---:|:---:|:---:|
|-|**/var/tmp/monitor-logs.json**|

### Dataprocessing service

|Listen to port|Log file location|
|:---:|:---:|:---:|
|5689|**/var/tmp/processingservice.log**|

### Couch DB

|Listen to port|Log file location|
|:---:|:---:|:---:|
|5984|-|

### Unit test couch DB

|Listen to port|Log file location|
|:---:|:---:|:---:|
|5980|-|



* All required python modules should be specified in **/vagrant/Adapters/requirements.txt**