<!--
  Please use the following template when creating a new issue. Please read the Bugs section of the Contributing guide before creating an issue.
-->

### Have you read the Bugs section of the Contributing guide?

(Input goes here.)


### Issue Type

<!--
  Please select the part of the code that the issue pertains to.
-->

1. Core module (i.e. data processing for issue trackers)
2. Adapter (include which adapter)
3. Django/Front end
4. VM Provisioning
5. Other (please specify)

Then, specify:

<!-- Which operating system are you using? Specify macOS, Windows, or Linux, along with specific release versions -->
- Development Operating System:

<!-- Include any additional relevant information. -->
- Build tools:

### Steps to Reproduce

<!--
  Please include the sequence of steps that will reproduce this issue. Be as specific as possible.
-->

(Write your steps here:)
1.
2.
3.

### Expected Behavior

<!--
  What was the expected behavior after engaging in the above steps?
-->

(Input for expected behavior goes here.)

### Actual Behavior

<!--
  What is the actual behavior after engaging in the above steps? Include screenshots if applicable.
-->

(Input actual behavior.)
