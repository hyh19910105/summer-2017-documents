# CouchDB Schema
## project_info
### Description
The **project_info** database stores the basic information of projects (JIRA/Bamboo/BitBucket). One project has one document.

### Fields Definition
* `_id`: project name set by the user on the **add project** page
* `name`: project name set by the user on the **add project** page
* `description`: project description set by the user on the **add project** page
* `adapter`: the name of the adapter which the project is from. It refers to the name in **schedular** database.
* `jira_id` / `bamboo_id` / `bitbucket_id`: the project id on the external server
* `attachments`: the attachment uploaded by the user on the **add project** page

## connectors
### Description
The **connectors** database stores information of running adapters (JIRA/Bamboo/BitBucket). 

### Fields Definition
* `_id`: adapter id
* `type`: adapter type (eg. issue tracker, code repo)
* `port`: the port which the adapter listens to

## schedular
### Description
The **schedular** database stores credentials to the external servers. 

### Fields Definition
* `_id`: identifier
* `host`: host name
* `pname`: the adapter name set on the **add adapter** page
* `port`: the port it listens to
* `username`: the username on the external server
* `password`: the password
* `timeInterval`: the interval of pulling data from the external server
* `lastUpdate`: the timestamp when the last pulling of data is done

## sprints
### Description
The **sprints** database stores the raw information about sprints retrieved from external issue tracker systems.

### Fields Definition
* `_id`: sprint id on external server
* `start_date`: the start date of this sprint (DD/Month/YY  HH:MM AM/PM)
* `end_date`: the end date of this sprint (DD/Month/YY  HH:MM AM/PM)

## issues
### Description
The **issues** database stores raw data of issues retrieved from external issue tracker systems.

### Fields Definition
* `_id`: `project_i` + `_` + `issue_id`
* `status`: the status of the issue
* `resolve_date`: the timestamp when the issue is resolved (YYYY-mm-DD  HH:MM:SS)
* `logged`: whether the issue is logged
	* If it is logged, the value is 1.
	* If not, the value is 0.
* `due`: the due date of the issue (YYYY-mm-DD  HH:MM:SS)
* `creation_date`: the timestamp when the issue is created (YYYY-mm-DD  HH:MM:SS)
* `priority`: the priority of the issue 
* `assignee`: the user who is assigned with this issue
* `sprint`: the sprint id which this issue belongs to
* `summary`: the summary of this issue
* `estimate`: estimated story points
	* If there is no estimated story points, use the estimated effort.
	* If the effort is not estimated neither, the value is null.
	
## commits
### Description
The **commits** database stores raw data of issues retrieved from external code repositories.

### Fields Definition
* `_id`: `project_id` + `_` + `commit_id`
* `author`: the user id of the commit author
* `date`: the commit date (YYYY-mm-DD  HH:MM:SS)
* `added`: lines of code added
* `deleted`: lines of code deleted
* `diff`: the raw text of diff



## projects_data
### Description
The **projects_data** database stores processed data (eg. burndown chart, etc.) by dataprocessing module. Each document represents a chart or table shown in VDM.

### Document Naming

`Project_id` + `_` + `Chart_Name`

### Schema of Issue Tracker Charts

* **\_issue\_count\_by\_priority**

```
{
  "_id": "<project_id>_issues_count_by_priority",
  "_rev": "**",
  "high": 2,
  "medium": 6,
  "low": 1
}
```
* **\_case\_status**

```
{
  "_id": "<project_id>_case_status",
  "_rev": "**",
  "x_axis": [
    "usera",
    "userb"
  ],
  "y_axis_title": "Number of Issues",
  "x_axis_title": "Developers",
  "closed": [34,1],
  "in_progress": [2,0],
  "estimated": [32,1],
  "open": [3,1],
  "not_estimated": [7,1]
}
```
* **\_burndown**

```
{
  "_id": "<project_id>_burndown",
  "_rev": "**",
  "x_axis": "value",
  "y_axis_title": "Story Points",
  "x_axis_title": "Date",
  "estimated": [6,5,...,0],
  "remaining": [7,7,...,1],
  "categories": [
    "6/19/2017",
    "6/20/2017",
    ... ,
    "6/25/2017"
  ]
}
```
* **\_issues\_status**

```
{
  "_id": "<project_id>_issues_status",
  "_rev": "**",
  "progress": 5,
  "open": 10,
  "closed": 254
}
```
* **\_project\_progress**
 
```
{
  "_id": "<project_id>_project_progress",
  "_rev": "**",
  "progress": 93.5
}
```
* **\_workload**

```
{
  "_id": "<project_id>_workload",
  "_rev": "**",
  "categories": [
    "usera",
    "userb",
    "userc"
  ],
  "lists": [[20,16,20],[1,0,20],[0,0,1]]
}
```
* **\_project\_burndown**

```
{
  "_id": "<project_id>_project_burndown",
  "_rev": "**",
  "x_axis": "value",
  "y_axis_title": "Story Points",
  "x_axis_title": "Date",
  "estimated": [6,5,...,0],
  "remaining": [7,7,...,1],
  "categories": [
    "6/19/2017",
    "6/20/2017",
    ... ,
    "6/25/2017"
  ]
}
```
* **\_sprint\_case\_status**

```
{
  "_id": "<project_id>_sprint_case_status",
  "_rev": "**",
  "x_axis": [
    "usera",
    "userb"
  ],
  "y_axis_title": "Number of Issues",
  "x_axis_title": "Developers",
  "closed": [5,6],
  "in_progress": [2,2],
  "estimated": [8,8],
  "open": [1,2],
  "not_estimated": [0,2]
}
```
* **global\_issue\_status\_project**

```
{
  "_id": "global_issue_status_project",
  "_rev": "**",
  "closed": [
    254,100
  ],
  "progress": [
    5,10
  ],
  "open": [
    10,5
  ],
  "categories": [
    "jira", "jira"
  ]
}
```
* **global_workload**

```
{
  "_id": "global_workload",
  "_rev": "**",
  "categories": [
    "usera",
    "userb",
    "userc"
  ],
  "lists": [
    [16,20,16],
    [0,5,0]
  ]
}
```

### Schema of Code Repository Charts


* **\_commit\_case\_activity**

``` 
{
  "_id": "<project_id>_commit_case_activity",
  "_rev": "**",
  "last_commits": [
    [
      "2017-06-29 12:08:49",
      {
        "id": "<commit id 1>",
        "author": "usera"
      }
    ],
    [
      "2017-06-29 11:36:41",
      {
        "id": "<commit id 2>",
        "author": "usera"
      }
    ],
    ... ,
    [
      "2017-06-28 12:00:59",
      {
        "id": "<commit id 5>",
        "author": "userd"
      }
    ]
  ],
  "project_info_to_return": {
    "team_id": "<team_id>",
    "repo_id": "<repository_id>"
  }
}

```

* **\_repo\_activity**

```
{
  "_id": "<project_id>_code_repo_activity",
  "_rev": "**",
  "num": [
    4,
    7,  
    ... ,  
    4
  ],
  "y_axis_title": "Number of Commits",
  "x_axis_title": "Date",
  "categories": [
    "6/15/2017",
    "6/16/2017",
    ... ,
    "6/29/2017"
  ]
}
```

* **\_activity\_per**

```
{
  "_id": "<project_id>_activity_per",
  "_rev": "**",
  "x_axis_values": [
    "usera",
    "userb",
     ... ,
    "userz"
  ],
  "y_axis_values": [
    31,
    17,
    ... ,
    1
  ],
  "y_axis_title": "Number of Commits",
  "x_axis_title": "Team Members"
}
```

* **\_modification\_activity**

```
{
  "_id": "<project_id>_modification_activity",
  "_rev": "**",
  "added": [
    61,
    241,
    ... ,    
    870
  ],
  "deleted": [
    21,
    39,
    ... ,
    7
  ],
  "y_axis_title": "Lines of Code",
  "x_axis_title": "Date",
  "categories": [
    "6/15/2017",
    "6/16/2017",
    ... ,
    "6/29/2017"
  ]
}
```

* **modification\_activity\_per**

```
{
  "_id": "<project_id>_modification_activity_per",
  "_rev": "**",
  "added": [
    548,
    1420,
    ... ,
    1
  ],
  "deleted": [
    183,
    150,
    ... ,
    1
  ],
  "y_axis_title": "Lines of Code",
  "x_axis_title": "Team Members",
  "categories": [
    "usera",
    "userb",
    ...,
    "userz"
  ]
}
```







