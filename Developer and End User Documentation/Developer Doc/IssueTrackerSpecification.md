#Issue Tracker Specification

##Project Dashboard### Chart: Project Issue Status per Team Member
The status of issues assigned to each team member.
* Time span: Project lifetime* x axis: the user names of developers* y axis: the number of issues* Issue status (according to data in the **issues** database)	* `Closed`: issue[‘status’] = `Closed`	* `Open`: issue[‘status’] = `Open`	* `In Progress`: issue[‘status’] = `In Progress`	* `Estimated`: issue[‘estimate’] is not null	* `Without estimate`: issue[‘estimate’] is null

### Chart: Sprint Issue Status per Team MemberThe status of issues assigned to each team member in the latest sprint.
* Time span: The latest sprint* x axis: the user names of developers* y axis: the number of issues* Issue status (according to data in the **issues** database)	* `Closed`: issue[‘status’] = `Closed`	* `Open`: issue[‘status’] = `Open`	* `In Progress`: issue[‘status’] = `In Progress`	* `Estimated`: issue[‘estimate’] is not null	* `Without estimate`: issue[‘estimate’] is null

### Chart: Issue Distribution by PriorityThe numbers of issues broken down by priority. Only `open` or `in progress` issues are considered.
* Time span: Project lifetime* Priority (according to data in the ‘issues’ database)	* `High`: issue[‘priority’] = `Highest` or `High`	* `Medium`: issue[‘priority’] = `Medium`	* `Low`: issue[‘priority] = `Lowest` or `Low`

### Chart: Sprint Remaining Workload per Team MemberThe sum of remaining estimate assigned to each team member in the latest sprint of a project.
* Time span: The latest sprint* axis: the usernames of developers* Workload value (according to data in the **issues** database): The sum of `estimate` field of issues that assigned to a developer (If an issue is assigned with story points, the value will be the number of story points. Otherwise, the value will be the estimated hours, if set.), and issues are either `open` or `in progress`.

### Chart: Sprint Burndown ChartThe burndown chart of the latest sprint.
* Time span: The latest sprint* x axis: dates in the latest sprint* y axis: the total estimate values of unresolved issues in the latest sprint before the end of a day (according to data in the **issues** database)* Line `Plan`: The ideal sprint burndown chart* Line `Remaining`: The actual sprint burndown chart### Chart: Project Burndown ChartThe burndown chart of the project.
* Time span: Project Time* x axis: dates during the project time* y axis: the total estimate values of unresolved issues before the end of a day (according to data in the **issues** database)* Line `Plan`: The ideal project burndown chart* Line `Remaining`: The actual project burndown chart

## Organizational View### Chart: WorkloadThe sum of remaining estimate assigned to each developer in all the projects that are added to the application in current week.
* Time span: The current week* axis: the usernames of developers* Workload value (according to data in the **issues** database): 
	* The sum of `estimate` field of issues that assigned to a developer (If an issue is assigned with story points, the value will be the number of story points. Otherwise, the value will be the estimated hours, if set.), and issues are either open or in progress.### Chart: Issue Status by ProjectsThe status of issues in each project.
* Time span: Project(s) lifetime* x axis: the project name set in the **add project** page* y axis: the number of issues* Issue status (according to data in the **issues** database)	* `Closed`: issue[‘status’] = `Closed`	* `Open`: issue[‘status’] = `Open`	* `In Progress`: issue[‘status’] = `In Progress`