# VDM Integration Documentation / Overview

A VDM adapter plugin is basically an API wrapper that connects to  and retrieve data from external tool server. You can look at JIRA or Bamboo adapter module as reference to build your own.

## Table of Contents
* [Plugin structure](#plugin-structure)
* [Database Structure](#database-structure)
* [Integration Handout](#integration-handout)

## Plugin structure
|Litsen To| Required Inputs  | Description  |
|:---:|:---|:---|
| *Auth*  |  username | The username of the user account|
|		 |	 password  | The password of the user account|
|		 |	 host 		| The server url of the tool connect to|
|		 |  pname | The name of the adapter |
|		  | | |
|  | **Optional Inputs**  |  **Description** | 
|	|		lastUpdate		|	The last update parameter is used for APIs that supports partial qurey. For example, when we pass last update parameter to JIRA REST API, JIRA server will only  give the data after that date
|	|		timeInterval		|	The fetch time interval to external server
|		  | | |
|		  | **Required Inputs** | **Description**|
| *UpdateData*  |  username | The username of the user account|
|		 |	 password  | The password of the user account|
|		 |	 host 		| The server url of the tool connect to|
|		 |  pname | The name of the adapter |
|		  | | |
|  | **Optional Inputs**  |  **Description** | 
|	|		lastUpdate		|	The last update parameter is used for APIs that supports partial qurey. For example, when we pass last update parameter to JIRA REST API, JIRA server will only  give the data after that date
|	|		timeInterval		|	The fetch time interval to external server
|		  | | |
|		  | **Required Inputs** | **Description**|
| *CheckIfExist*  |  username | The username of the user account|
|		 |	 password  | The password of the user account|
|		 |	 host 		| The server url of the tool connect to|
|		 |  key | The project identification code |
|		  | | |
|  | **Optional Inputs**  |  **Description** | 
| *Cancel*  |  pname | The name of the adapter|


## Database structure
For different type of adapter, the information will be stored into different database in CouchDB. Data processing service will process data from the following information and calculate graph.


JIRA adapter saved information to two different databse, sprint and issue.   
Sprint DB:

```json
{
  "_id": "DEV SPRINT 1",
  "_rev": "225-asdasd",
  "start_date": "01/Jun/17 11:50PM",
  "end_date": "14/Jun/17 11:50PM"
}
```

Issue DB:

```json
{
  "_id": "225-asdasd",
  "_rev": "225-asdasd",
  "status": "In Progress",
  "assignee": "hyh19910105",
  "resolve_date": "2017-07-05T16:45:32.000-400",
  "due": NULL,
  "sprint": "DEV SPRINT 1",
  "creation_date": "2017-07-05T16:45:32.000-400",
  "logged": 22,
  "estimate": 13,
  "priority": "High",
  "summary": "A summary of the sprint"
}
```

Bitbucket adapter saved information to commits database.   
Commits DB:

```json
{
  "_id": "225-asdasd",
  "_rev": "225-asdasd",
  "author": "hyh19910105",
  "date": "2017-07-05T16:45:32.000-400",
  "diff": "+++ node.js"
}
```

Bamboo adapter saved information to builds database.   
Builds DB:

```json
{
	"_id":"VDM-VB-45","_rev":"1-02e29fc0324a8182161776da84b50959",
	"reasonSummary":"Changes by <a href=\"http://128.2.24.123:8085/users/viewUserSummary.action?currentUserName=huangyuhen\">Yuheng Huang</a>",
	"key":"VDM-VB-45",
	"comments_size":0,
	"quarantinedTestCount":0,
	"number":45,
	"prettyBuildStartedTime":"Wed, 26 Jul, 11:18 AM",
	"prettyBuildCompletedTime":"Wed, 26 Jul, 11:18 AM",
	"id":688217,
	"buildRelativeTime":"5 days ago",
	"notRunYet":false,
	"buildCompletedTime":"2017-07-26 07:18:06",
	"state":"Successful",
	"buildCompletedDate":"2017-07-26 07:18:06",
	"continuable":false,
	"restartable":false,
	"successful":true,
	"projectName":"VDM",
	"buildDuration":1069,
	"buildState":"Successful",
	"buildNumber":45,
	"finished":true,
	"failedTestCount":0,
	"buildTestSummary":"No tests found",
	"plan":{
		"name":"VDM - VDM BAMBOO",
		"key":"VDM-VB",
		"link_href":"http://128.2.24.123:8085/rest/api/latest/plan/VDM-VB",
		"planKey":"VDM-VB",
		"shortName":"VDM BAMBOO",
		"shortKey":"VB"
		},
	"planResultKey":"VDM-VB-45",
	"skippedTestCount":0,
	"planName":"VDM BAMBOO",
	"buildDurationInSeconds":1,
	"buildStartedTime":"2017-07-26 07:18:05",
	"successfulTestCount":0,
	"buildReason":"Changes by <a href=\"http://128.2.24.123:8085/users/viewUserSummary.action?currentUserName=huangyuhen\">Yuheng Huang</a>",
	"lifeCycleState":"Finished",
	"buildDurationDescription":"1 second",
	"link_href":"http://128.2.24.123:8085/rest/api/latest/result/VDM-VB-45","buildResultKey":"VDM-VB-45"
	}
```

## Integration Handout
* All adapter must be implement be implement under **/Vagrant/Adapters/**
* The connection to CouchDB is on localhost port 5984
* All adapter should send heart beat to VDM monitor service every 30 seconds, the is also served as a registration process to register adapter to web app.
	* A heart beat should be a post request send to local host 5689
	* The post body of heart beat should be following format:
	
```json
{
  "status": "alive",
  "port": "5656" (The port the adapter is running),
  "connector": "JIRA"  (The adapter name)
}
```
	
* After the adapter is implemented, the new adapter should be added to **/Vagrant/Monitor/setup/connectors.json**, providing adapter name, port which is running on, and type (issue trackers, build servers or code repository)
* Use the service template in /Adapters/Vagrant/setup folder and update $DAEMON, $ARGS and $PID
* Modify vagrant file in the main folder
  - Go to connectors section ##connectors
  - Add following lines:

         *sudo cp setup/YourConnector /etc/init.d/*
     
         *sudo chmod +x /etc/init.d/YourAdapter*
* Use uWSGI framework on top of your adapter to be able to run multiple adapter at the same time. 
* Your uWSGI framework configuration file should be created under **/vagrant/Adapters/setup/**. The configuration file should be following format:   

``` 
[uwsgi]
http-socket = :5656
chdir = /vagrant/Adapters/
plugin    = python
wsgi-file = /vagrant/Adapters/jiraserver.py
processes = 4
threads = 4
callable = app

```
