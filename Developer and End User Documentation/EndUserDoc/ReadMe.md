## Synopsis

Industry leaders manage projects by using different issue tracking, code repositories, and other DevOps tools of the like. The Virtual Development Manager provides a birds-eye view of projects in a centralized dashboard. With the help of VDM, project managers can view the data aggregations and visualizations to better inform project progress and decisions.


## Installation

**Prerequisite:** 

* [Vagrant](https://www.vagrantup.com/downloads.html) 
* [Virtual box](https://www.virtualbox.org/wiki/Downloads) 
* [VDM Source Code](https://bitbucket.org/virtualdevmanager/code)

**Steps to get VDM running:**

1. Open terminal
2. **cd** to your cloned source code directory
3. Verify you have a file called Vagrantfile in your current directory  
![cdDirectory.jpg](https://bitbucket.org/repo/5rxnGr/images/4166544299-cdDirectory.jpg)
4. Run command: **vagrant up**  
Once you see followings screen, your VDM should be ready to run
![UpRunning.png](https://bitbucket.org/repo/5rxnGr/images/3388200992-UpRunning.png)
5. Type **http://localhost:8000/** in your browser which you will be directed to VDM screen
Here is a screen-shot of VDM main screen 
![MainScreen](Images/MainScreen.png)
6. Click **Connectors** to create new connector then the page will navigate to following screen</br>
![Connector.png](Images/Connector.png)
7. Click **Add** button  
![Add.png](Images/Add.png)
Currently, the port only support JIRA and bamboo.  
*If you find that port is not selectable.*  
Type:  
**vagrant ssh**   
Then type:  
**sudo service VDMmonitor restart**  
Give 1 or 2 min for connector to start, then you will be able to select port number.  
Username: *your jira/bamboo username*   
Password: *your jira/bamboo password*   
Hosting server: *Example https://devman.atlassian.net*  
Port: jira or bamboo  
Adaptor name: *A nickname for adaptor*  
Interval: Automatic data fetching interval time  

8. Once you entered all info and click submit, you will get following screen
![Success.png](Images/Success.png)
9. Click Home button to navigate back to home page
10. You should be able to create new project using the adapter you just created.
11. Click Add project icon (folder like thing). Then you will get following screen:  
![AddProject.png](Images/AddProject.png)
![ProjectCreate.png](Images/ProjectCreate.png)
12. Fill the name for the project and description.  
Notice that when you select JIRA, you will be prompt for **JIRA ID**   
![JIRAID.png](Images/JIRAID.PNG)  
You will be able to find your JIRA ID under your project view under key column. For example, I want to monitor VirtualDevManager project, and the JIRA ID is VDM.
![AllProjectJIRA.png](Images/AllProjectJIRA.png)
13. The VDM might require some time (Depending on the size of your project) to fetch the data from server. Refresh the browser after few minutes, then your project data should be ready!   
![ProjectReady.png](Images/ProjectReady.png)
![ProjectReady-2.png](Images/ProjectReady-2.png)
![ProjectConfigurationPage-2.png](Images/ProjectConfigurationPage-2.png)

## Contributors
**Thanks goes to these wonderful people:**  

Team Leader  | Client
------------- | -------------
![Hasan.jpg](https://bitbucket.org/repo/5rxnGr/images/84308446-Hasan.jpg)  | ![Tim.png](https://bitbucket.org/repo/5rxnGr/images/1538555892-Tim.png)
[Hasan Yasar](https://www.linkedin.com/in/hasanyasar/)  | [Tim Palko](https://www.linkedin.com/in/tim-palko-37431913/)



Developer  |  Developer |  Developer |Developer |Developer |Developer |Developer 
:-------------:| :-------------: |:-------------: |:-------------: |:-------------: |:-------------: |:-------------: |
![LindaWu.png](https://bitbucket.org/repo/5rxnGr/images/1553320768-LindaWu.png)  |  ![XiaoleiPeng.jpg](https://bitbucket.org/repo/5rxnGr/images/316486460-XiaoleiPeng.jpg) |  ![YuhengHuang.png](https://bitbucket.org/repo/5rxnGr/images/833496935-YuhengHuang.png)  |   ![Gunjan.jpg](https://bitbucket.org/repo/5rxnGr/images/264520171-Gunjan.jpg) |  ![XiaoBao.jpg](https://bitbucket.org/repo/5rxnGr/images/3311824220-XiaoBao.jpg) | ![Anurag.jpg](https://bitbucket.org/repo/5rxnGr/images/3122873134-Anurag.jpg) |   ![ShengRong.jpg](https://bitbucket.org/repo/5rxnGr/images/2921212464-ShengRong.jpg) |   
[Linda Wu](https://www.linkedin.com/in/lwu613/) | [Xiaolei Peng](https://www.linkedin.com/in/xiaolei-peng-65bb43104/)| [Yuheng Huang](https://www.linkedin.com/in/yuhengh/)| [Gunjan Raghav](https://www.linkedin.com/in/gunjan-raghav-1520131b/)| [Xiao Bao](https://www.linkedin.com/in/xiao-bao-14a810110/)|[Anurag Kanungo](https://www.linkedin.com/in/anuragkanungo/) | [Shengrong Liu](https://www.linkedin.com/in/shengrong-liu-0a458b57/) | 

## License

VDM is licensed under the Apache License 2.0.